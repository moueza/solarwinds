/*
 * Triangle.h
 *
 *  Created on: 4 août 2013
 *      Author: peter
 */

#ifndef TRIANGLE_H_
#define TRIANGLE_H_
#include <string>
#include <iostream>
using namespace std;//pr dtring
class Triangle {
public:
	Triangle();
	int integ();
	string print();
	virtual ~Triangle();
};

#endif /* TRIANGLE_H_ */
